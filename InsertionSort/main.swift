//  main.swift
//  InsertionSort
//
//  Created by Daniel Buki on 4/29/16.

import Foundation

let unsorted: [Int] = [6, 3, 1, 5, 4, 10, 2, 19, 102, 98, -6, -5, 17]


insertSort().sortAndShowOutput(unsorted)
bubbleSort().sortAndShowOutput(unsorted)






























func firstAndLastName(_ fullName: String) -> (String, String) {

    var components = fullName.components(separatedBy: " ")
    
    return (components[0], components[1])
    
}

let fullName = "Asha Mandava"
var (firstName, lastName) = firstAndLastName(fullName)

print("First name: \(firstName)\tLast name: \(lastName)")
