//
//  sorterProtocol.swift
//  Sorting
//
//  Created by Daniel Buki on 5/6/16.
//  Copyright © 2016 Daniel Buki. All rights reserved.
//

import Foundation

protocol sorterProtocol {
    
    static var sortAlgorithmName: String { get }
    
    func sort(_ unsortedList: [Int]) -> (sortAlgorithm: String, sortedList: [Int], executionStepCount: Int)
    
}

extension sorterProtocol {
    
    func sortAndShowOutput(_ unsortedList: [Int]) -> Void
    {
        print("Unsorted list: " + String(unsortedList))
        let (sortType, result, steps) = self.sort(unsortedList)
        print("  Sorted list: " + String(result))
        print("\nRequired " + String(steps) + " steps using \(sortType)\n\n")
    }
    
}
